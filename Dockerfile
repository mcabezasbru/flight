FROM golang:1.12-alpine3.10 AS build-env

ENV PORT 8080
ENV APP_NAME flight-app

#Allow Go to retrieve the dependencies for the build step
RUN apk add --no-cache git

#Secure against running as root
RUN adduser -D -u 10000 bbrunner
RUN mkdir /bbapp/ && chown bbrunner /bbapp/
USER bbrunner

WORKDIR /bbapp/
ADD . /bbapp/

# Compile the binary
RUN go get ./
RUN CGO_ENABLED=0 go build -o ${APP_NAME} .

#final stage
FROM alpine:3.10

#Secure against running as root
RUN adduser -D -u 10000 bbrunner
USER bbrunner

WORKDIR /
COPY --from=build-env /bbapp/${APP_NAME} /

EXPOSE ${PORT}

CMD ["./flight-app"]