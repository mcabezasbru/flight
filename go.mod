module bitbucket.org/mcabezasbru/flight

go 1.12

require (
	github.com/go-kit/kit v0.9.0
	github.com/gorilla/mux v1.7.3
	github.com/prometheus/client_golang v1.3.0
	github.com/satori/go.uuid v1.2.0
)
