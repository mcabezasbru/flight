# flight

##### Flight-Reservation ENDPOING
* Simple Request
    

    curl localhost:8080/flight-reservations
* Traceable Request

    
    curl localhost:8080/flight-reservations?requestID=12
    
##### Store
* Simple Request
    
    
    curl -X POST localhost:8080/flight-reservations -d '{"Destination":"caca", "ReservationID":"justCreatedUUID"}'
* Traceable Request


    curl -X POST localhost:8080/flight-reservations -d '{"requestID":"12", "reservation": {"destination":"caca", "reservation_id":"justCreatedUUID"}}'
### Kubernetes Gracefully Shutdown Support

This microservice supports the following shutdown signals

* os.Interrupt
* syscall.SIGINT
* syscall.SIGTERM

### Run with composer
    docker-compose up --build

### Experimental Kubernetes deployment file

The kubernates deployment file was not tested
