package main

import (
	"context"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/mcabezasbru/flight/reservation"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"

	"github.com/go-kit/kit/log"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/gorilla/mux"

)


func main() {
	flag.Parse()
	var logger log.Logger
	logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	var (
		reservations = reservation.NewRepository()
	)
	// Facilitate testing by adding some reservations.
	storeTestData(reservations)
	fieldKeys := []string{"method"}
	var rs reservation.Service
	rs = reservation.NewService(reservations)
	rs = reservation.NewLoggingService(log.With(logger, "component", "reservation"), rs)
	rs = reservation.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "reservation_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "reservation_service",
			Name:      "request_latency_microseconds",
			Help:      "Total duration of requests in microseconds.",
		}, fieldKeys),
		rs,
	)
	httpLogger := log.With(logger, "component", "http")
	handler := mux.NewRouter()
	handler.Handle("/flight-reservations", reservation.MakeHandler(rs, httpLogger))
	handler.Handle("/metrics", promhttp.Handler())
	handler.HandleFunc("/healthz", healthzHandler())
	handler.HandleFunc("/readiness", readinessHandler())
	srv := &http.Server{
		Handler:      handler,
		Addr:         ":" + envString("PORT", "8080"),
		ReadTimeout:  60 * time.Second,
		WriteTimeout: 60 * time.Second,
	}
	// Start Server
	go func() {
		logger.Log("INFO", "Starting Server")
		if err := srv.ListenAndServe(); err != nil {
			logger.Log("INFO",err)
		}
	}()
	waitForShutdown(srv, logger)
}

func envString(env, fallback string) string {
	e := os.Getenv(env)
	if e == "" {
		return fallback
	}
	return e
}

func waitForShutdown(srv *http.Server, logger log.Logger) {
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	// Block until we receive our signal.
	<-interruptChan
	// Create a deadline to wait for.
	logger.Log("INFO", "Shutdown signal received")
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*60)
	defer cancel()
	srv.Shutdown(ctx)
	logger.Log("INFO", "Gracefully shutting down")
	os.Exit(0)
}

func storeTestData(r reservation.Repository) {
	test1 := &reservation.Reservation{
		Date:          time.Now(),
		Destination:   "Congo",
		ReservationID: "12",
	}
	r.Store(context.Background(), test1)
	test2 := &reservation.Reservation{
		Date:          time.Now(),
		Destination:   "Congo2",
		ReservationID: "122",
	}
	r.Store(context.Background(), test2)
}

func healthzHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte("We are alive!"))
	}
}

func readinessHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		w.Write([]byte("We are ready!"))
	}
}