package reservation

import (
	"context"
	"sync"
)

type Repository interface {
	FindAll() []*Reservation
	Store(ctx context.Context, reservation *Reservation) error
}

type repository struct {
	reservations *sync.Map
}

func NewRepository() Repository {
	return &repository{
		reservations: &sync.Map{},
	}
}

func (r *repository) FindAll() []*Reservation {
	var reservations []*Reservation
	r.reservations.Range(func(k, v interface{}) bool {
		reservations = append(reservations, v.(*Reservation))
		return true
	})
	return reservations
}

func (r *repository) Store(ctx context.Context, reservation *Reservation) error {
	r.reservations.Store(reservation.ReservationID, reservation)
	return nil
}