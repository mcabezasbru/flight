package reservation

import (
	"context"
	"errors"

	"github.com/go-kit/kit/endpoint"
)

type reservationsRequest struct {
	RequestID string
}

type reservationsResponse struct {
	RequestID    UUID           `json:"requestID"`
	Reservations []*Reservation `json:"reservations"`
	Err          error          `json:"error,omitempty"`
}

func (r reservationsResponse) error() error { return r.Err }

func makeReservationsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		if req, ok := request.(*reservationsRequest); ok {
			reservations, err := s.Reservations(ctx, req.RequestID)
			return reservationsResponse{RequestID: UUID(req.RequestID), Reservations: reservations, Err: err}, nil
		}
		if req, ok := request.(*storeRequest); ok {
			err := s.Store(ctx, req.RequestID, &req.Reservation)
			return storeResponse{RequestID: UUID(req.RequestID), Err: err}, nil
		}
		return nil, errors.New("UNSUPPORTED_REQUEST")
	}
}

type storeRequest struct {
	RequestID   string
	Reservation Reservation
	Ctx         context.Context
}

type storeResponse struct {
	RequestID UUID  `json:"requestID"`
	Err       error `json:"error,omitempty"`
}
