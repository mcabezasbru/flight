package reservation

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"bitbucket.org/mcabezasbru/flight/util"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/transport"
)

// MakeHandler returns a handler for the booking service.
func MakeHandler(rs Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(transport.NewLogErrorHandler(logger)),
		kithttp.ServerErrorEncoder(encodeError),
	}
	reservationsHandler := kithttp.NewServer(
		makeReservationsEndpoint(rs),
		decodeFlightReservationsRequest,
		encodeResponse,
		opts...,
	)
	r := mux.NewRouter()
	r.Handle("/flight-reservations", reservationsHandler).Methods("GET", "POST")
	return r
}

func decodeFlightReservationsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	if r.Method == http.MethodGet {
		return decodeGet(r)
	}
	if r.Method == http.MethodPost {
		return decodePost(r)
	}
	return nil, errors.New("UNEXPECTED_HTTP_METHOD")
}

func decodeGet(r *http.Request) (*reservationsRequest, error) {
	var reqID string
	params, ok := r.URL.Query()["requestID"]
	if !ok || len(params[0]) < 1 {
		reqID = util.UUID()
	} else {
		reqID = params[0]
	}
	return &reservationsRequest{RequestID: reqID}, nil
}

func decodePost(r *http.Request) (*storeRequest, error) {
	var body struct {
		RequestID   string      `json:"requestID"`
		Reservation Reservation `json:"reservation"`
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		return &storeRequest{}, err
	}
	if body.RequestID == "" {
		body.RequestID = util.UUID()
	}
	return &storeRequest{
		RequestID:   body.RequestID,
		Reservation: body.Reservation,
		Ctx:         r.Context(),
	}, nil
}

type errorer interface {
	error() error
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// encode errors from business-logic
func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	switch err {
	case ErrInvalidArgument:
		w.WriteHeader(http.StatusBadRequest)
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}
