package reservation

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) Reservations(ctx context.Context, requestID string) ([]*Reservation, error) {
	var err error
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "reservations",
			"requestID", requestID,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	reservations, err := s.Service.Reservations(ctx, requestID)
	return reservations, err
}

func (s *loggingService) Store(ctx context.Context, requestID string, reservation *Reservation) error {
	var err error
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "store",
			"requestID", requestID,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	err = s.Service.Store(ctx, requestID, reservation)
	return err
}