package reservation

import (
	"context"
	"time"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(counter metrics.Counter, latency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   counter,
		requestLatency: latency,
		Service:        s,
	}
}

func (s *instrumentingService) Reservations(ctx context.Context, requestID string)([]*Reservation, error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "reservations").Add(1)
		s.requestLatency.With("method", "reservations").Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.Reservations(ctx, requestID)
}

func (s *instrumentingService) Store(ctx context.Context, requestID string, reservation *Reservation)  error {
	defer func(begin time.Time) {
		s.requestCount.With("method", "store").Add(1)
		s.requestLatency.With("method", "store").Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.Store(ctx, requestID, reservation)
}