package reservation

import (
	"context"
	"errors"
	"time"
)

var ErrInvalidArgument = errors.New("invalid argument")

type Service interface {
	Reservations(ctx context.Context, requestID string) ([]*Reservation, error)
	Store(ctx context.Context, requestID string, reservation *Reservation) error
}

type UUID string

type Reservation struct {
	Date          time.Time `json:"date"`
	Destination   string    `json:"destination"`
	ReservationID UUID      `json:"reservation_id"`
}

type service struct {
	reservations Repository
}

func (s *service) Reservations(ctx context.Context, requestID string) ([]*Reservation, error) {
	return s.reservations.FindAll(), nil
}

func (s *service) Store(ctx context.Context, requestID string, reservation *Reservation) error {
	return s.reservations.Store(ctx, reservation)
}

// NewService creates a reservation service with necessary dependencies.
func NewService(repository Repository) Service {
	return &service{
		reservations: repository,
	}
}
